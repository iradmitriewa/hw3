package exercise3;

import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import static java.util.stream.Collectors.*;

public class Counter {

    public static void count(String str){
        Map<Character, Integer> map = new HashMap<>();
        for(int i=0;i<str.length();i++) {
            Character a = str.charAt(i);
            Integer count = map.get(a);
            if (count == null) {
                count = 1;
                map.put(a, count);
            } else {
                count += 1;
                map.put(a, count);
            }
        }
        Map<Character, Integer> sortMap = map;
        sortMap = map
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1,e2)->e2, LinkedHashMap::new));
        System.out.println(sortMap);
    }

    public static void main(String[] args) {
        String str = "Сбер - лучший банк мира";
        count(str);
    }
}
