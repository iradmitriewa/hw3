package exercise2;

import java.util.*;

public class Converter {

    public static boolean isOperator(char op){
        if (op == '+' || op == '-' || op == '*' || op == '/'){
            return true;
        }
        return false;
    }

    public static boolean isParentheses(char par){
        if (par == '(' || par == ')'){
            return true;
        }
        return false;
    }

    public static boolean isDigit(char number){
        return Character.isDigit(number);
    }

    public static int isPriority(char op){
        if (op == '(' || op == ')') {
            return 3;
        } else if (op == '*' || op == '/'){
            return 2;
        } else if (op == '+' || op == '-'){
            return 1;
        }
       return 4;
    }

    public static void transfer(String expr){
        ArrayDeque<Character> stack = new ArrayDeque<Character>();
        Stack<Character> buffer = new Stack<>();
        char[] arrayExpr = expr.toCharArray();
        int length = arrayExpr.length;
        for (int i =0; i<length; i++){
            if (isDigit(arrayExpr[i])){
                System.out.println(arrayExpr[i]);
                //System.out.println(stack.pop());
            } else if (isParentheses(arrayExpr[i])){
                if (arrayExpr[i] == '(') {
                    stack.push(arrayExpr[i]);
                }
                if (arrayExpr[i] == ')') {
                    stack.push(arrayExpr[i]);
                    while (!stack.isEmpty() && stack.peek() != '('){
                        System.out.println(stack.pop());
                    }
                }
            } else if (isOperator(arrayExpr[i])){
                while (!stack.isEmpty() && isPriority(arrayExpr[i]) <= isPriority(stack.peek())){
                    System.out.println(stack.pop());
                }
                stack.push(arrayExpr[i]);
            }
        }
        while (!stack.isEmpty()){
            if (isOperator(stack.peek())){
                System.out.println(stack.pop());
            }
        }
    }

    public static void main(String[] args) {
        String expression = "1+2*4+3";
        transfer(expression);
    }
}