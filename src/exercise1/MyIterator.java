package exercise1;

import java.util.Arrays;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class MyIterator implements Iterable<String>{

    private String[] arrayString;
    private int index;
    private int count = arrayString.length;

    public MyIterator(String[] arrayString, int index, int count) {
        this.arrayString = arrayString;
        this.index = index;
        this.count = count;
    }

    @Override
    public Iterator<String> iterator() {
        Iterator<String> iter = new Iterator<String>() {
            @Override
            public boolean hasNext() {
                return index<count;
            }

            @Override
            public String next() {
                if(index<count){
                    return arrayString[index++];
                } else {
                    throw new NoSuchElementException("Elements is end");
                }
            }

        };
        return iter;
    }

    public static void main(String[] args) {
        String[] array = new String[5];
        array[0] = "1";
        array[1] = "2";
        array[2] = "3";
        array[3] = "4";

        Iterator<String> iter = Arrays.stream(array).iterator();


        while(iter.hasNext()){
            System.out.println(iter.next() + " ");
        }

    }
}
